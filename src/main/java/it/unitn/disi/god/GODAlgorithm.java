package it.unitn.disi.god;

import java.util.List;

import de.metanome.algorithm_integration.AlgorithmExecutionException;
import de.metanome.algorithm_integration.ColumnIdentifier;
import de.metanome.algorithm_integration.ColumnPermutation;
import de.metanome.algorithm_integration.input.InputGenerationException;
import de.metanome.algorithm_integration.input.InputIterationException;
import de.metanome.algorithm_integration.input.RelationalInput;
import de.metanome.algorithm_integration.input.RelationalInputGenerator;
import de.metanome.algorithm_integration.result_receiver.CouldNotReceiveResultException;
import de.metanome.algorithm_integration.result_receiver.OrderDependencyResultReceiver;
import de.metanome.algorithm_integration.results.OrderDependency;
import it.unitn.disi.god.core.Candidate;
import it.unitn.disi.god.utils.Combinations;
import it.unitn.disi.god.utils.Permutations;
import it.unitn.disi.god.core.Column;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GODAlgorithm {

    protected RelationalInputGenerator inputGenerator = null;
    protected OrderDependencyResultReceiver resultReceiver = null;
    protected String relationName;
    protected List<String> columnNames;
    protected int numberOfColumns;
    protected ArrayList<Column> dataset;
    protected ArrayList<Integer> columnsIds;
    HashMap<String, List<String>> lgoods;
    HashMap<String, List<String>> rgoods;
    private int holdCandidates;
    private static int NUM_THREADS = 1;

    public void execute() throws AlgorithmExecutionException {
        ////////////////////////////////////////////
        // THE DISCOVERY ALGORITHM LIVES HERE :-) //
        ////////////////////////////////////////////

        // To test if the algorithm gets data
//        this.print();
        // To test if the algorithm outputs results
//        this.outputSomething();
        System.out.println("Initialization...");
        init();
        System.out.println("Done initialization!\n"); // TESTING

        List<Candidate> startingCandidates = generateCandidates(2);
        ConcurrentLinkedQueue<Candidate> candidates = new ConcurrentLinkedQueue<>(startingCandidates);
//        ConcurrentLinkedQueue<Candidate> toExpand;
        ConcurrentSkipListSet<Candidate> toExpand;
//        LinkedHashSet<Candidate> uniqCands;

        int parsedCandidates = 0;
        ExecutorService pool = Executors.newFixedThreadPool(GODAlgorithm.NUM_THREADS);
        while (!candidates.isEmpty()) {
            toExpand = new ConcurrentSkipListSet<>();

            List<Future<Integer>> lists = new ArrayList<>();
            for (int thNum = 0; thNum < GODAlgorithm.NUM_THREADS; thNum++) {
                ODValidityChecker chk = new ODValidityChecker(thNum, candidates, toExpand, this.dataset);
                lists.add(pool.submit(chk));
            }

            //Merge partial results
            try {

                Integer ptmp;
                for (Future<Integer> parsed : lists) {
                    ptmp = parsed.get();
                    if (ptmp != null) {
                        parsedCandidates += ptmp;
                    }
                }
                System.out.println("Total Parsed Candidats: "+ parsedCandidates);
                System.out.println("Total Found Candidats: "+ this.holdCandidates);
            } catch (InterruptedException | ExecutionException ex) {
                //System.err.println(ex.toString());
                ex.printStackTrace();

            }

            System.out.println(); //TESTING
//            System.out.print("Generating new candidates... ");
//            uniqCands = new ConcurrentSkipListSet<>();
//            uniqCands = new LinkedHashSet<>();

//            for (Candidate cand : toExpand) {
//                List<Candidate> newCandidates = cand.expand(this.columnsIds, this.dataset);
//                uniqCands.addAll(newCandidates);
//            }

            candidates.addAll(toExpand);
            System.out.println("Generated " + candidates.size() + " new candidates!"); //TESTING
//            System.out.println("Holded candidates: " + this.holdCandidates); //TESTING
        }
        pool.shutdown();
    }

    protected void init() throws AlgorithmExecutionException {
        this.holdCandidates = 0;
        System.out.print("Loading dataset... ");
        RelationalInput input = this.inputGenerator.generateNewCopy();
        System.out.println("Done!");
        this.relationName = input.relationName();
        this.columnNames = input.columnNames();
        this.numberOfColumns = input.numberOfColumns();
        System.out.println("Number of columns: " + this.numberOfColumns); // TESTING
        this.dataset = readDataset(input);
        this.lgoods = new HashMap<>(this.numberOfColumns);
        this.rgoods = new HashMap<>(this.numberOfColumns);

        this.columnsIds = new ArrayList<>(numberOfColumns);

        for (int i = 0; i < numberOfColumns; i++) {
            this.columnsIds.add(i);
        }

        Properties prop = new Properties();
        InputStream inputProperty = null;

        try {
            inputProperty = new FileInputStream("config.properties");
            prop.load(inputProperty); // load a properties file

            // get the property value and print it out
            GODAlgorithm.NUM_THREADS = Integer.parseInt(prop.getProperty("NUM_THREADS"));
        } catch (IOException ex) {
            System.err.println("File: \"config.properties\" not found, using default values");
        } finally {
            if (inputProperty != null) {
                try {
                    inputProperty.close();
                } catch (IOException e) {
                    System.err.println("Male, molto male!");
                }
            }
        }

        System.out.println("Threads: " + GODAlgorithm.NUM_THREADS);
    }

    protected List<Candidate> generateCandidates(int combSize) throws InputIterationException, InputGenerationException {

        List<Candidate> results = new ArrayList<>(numberOfColumns);

        Combinations<Integer> combs = new Combinations<>(this.columnsIds, combSize);

        Candidate cn;
        int cdisc = 0;

        for (List<Integer> comb : combs) {

            for (ArrayList<Integer> s : Permutations.permutations(comb)) { // non interator!

                for (int i = 1; i < combSize; i++) {
                    List<Integer> left = s.subList(0, i);
                    List<Integer> right = s.subList(i, combSize);
//                    String leftSig = computeSignature(left);
//                    String rightSig = computeSignature(right);
//                    boolean discard = false;
//
//                    // PRUNING! se left (fino a i) e' contenuto in roba "buona"
//                    if (this.lgoods.containsKey(leftSig)) {
//                        List<String> rights = this.lgoods.get(leftSig);
//
//                        for (String r : rights) {
//                            if (rightSig.startsWith(r)) {
//                                discard = true;
//                                cdisc++;
//                                break;
//                            }
//                        }
//                    } else if (this.rgoods.containsKey(rightSig)) {
//                        List<String> lefts = this.rgoods.get(rightSig);
//
//                        for (String l : lefts) {
//                            if (rightSig.startsWith(l)) {
//                                discard = true;
//                                cdisc++;
//                                break;
//                            }
//                        }
//                    } else {
//                        // prefix in both left and right hand-sides
//                    }
//
//                    if (!discard) {
                    cn = new Candidate();
                    cn.setlHSFieldsList(left);
                    cn.setrHSFieldsList(right);

                    results.add(cn);
//                    } else {
//                        break;
//                    }
                }
            }
        }

        System.out.println("Skipped: " + cdisc); // TESTING

        return results;
    }

    protected void print() throws InputGenerationException, InputIterationException {
        RelationalInput input = this.inputGenerator.generateNewCopy();
        int length = 2;
        System.out.print(this.relationName + ":\n|\t");
        for (String columnName : this.columnNames) {
            System.out.print(columnName + "\t");
            length += columnName.length() + 1;
        }
        length += 2;
        System.out.println("\t|");
        System.out.println();

        while (input.hasNext()) {
            System.out.print("|\t");

            List<String> record = input.next();
            for (String value : record) {
                System.out.print(value + "\t|\t");
            }

            System.out.println();
        }
    }

    protected void print(ArrayList<Column> alsc) {
        int nRows = alsc.get(0).getnRows();

        for (Column alsc1 : alsc) {
            System.out.print(alsc1.getColumnTypeName() + " ");
        }
        System.out.println("");
        for (int i = 0; i < nRows; i++) {
            for (Column alsc1 : alsc) {
                System.out.print(alsc1.getValues().get(i) + " ");
            }
            System.out.println("");
        }
    }

    protected ArrayList<Column> readDataset(RelationalInput input) throws InputIterationException {
        System.out.print("Reading dataset... ");

        ArrayList<ArrayList<String>> datasett = new ArrayList<>(numberOfColumns);
        ArrayList<Column> ret = new ArrayList<>();

        for (int i = 0; i < numberOfColumns; i++) {
            datasett.add(new ArrayList<String>(numberOfColumns * 2));
        }

        while (input.hasNext()) {
            List<String> record = input.next();
            int c = 0;

            for (String value : record) {
                datasett.get(c++).add(value);
            }
        }

        for (ArrayList<String> lists : datasett) {
            ret.add(new Column(lists));
        }

        System.out.println("Done!");
        return ret;
    }

    protected synchronized void holdCandidate(Candidate candidate) throws CouldNotReceiveResultException {
        this.holdCandidates++;
        OrderDependency od = new OrderDependency(
                getColumnPermutation(candidate.getlHSFieldsList()),
                getColumnPermutation(candidate.getrHSFieldsList()),
                OrderDependency.OrderType.LEXICOGRAPHICAL,
                OrderDependency.ComparisonOperator.STRICTLY_SMALLER);

//        String lsig = candidate.getLeftSignature();
//        String rsig = candidate.getRightSignature();
//        List<String> ltmp = this.lgoods.get(lsig);
//        List<String> rtmp = this.rgoods.get(lsig);
//
//        if (ltmp == null) {
//            ltmp = new ArrayList<>();
//        }
//        if (rtmp == null) {
//            rtmp = new ArrayList<>();
//        }
//
//        ltmp.add(rsig);
//        rtmp.add(lsig);
//        this.lgoods.put(lsig, ltmp);
//        this.rgoods.put(rsig, rtmp);
        this.resultReceiver.receiveResult(od);
    }

    private ColumnPermutation getColumnPermutation(List<Integer> columnIds) {
        List<ColumnIdentifier> ids = new ArrayList<>();
        ColumnPermutation cp = new ColumnPermutation();

        for (Integer i : columnIds) {
            ids.add(new ColumnIdentifier(this.relationName, this.columnNames.get(i)));
        }

        cp.setColumnIdentifiers(ids);
        return cp;
    }

    public static String computeSignature(List<Integer> li) {
        StringBuilder sb = new StringBuilder();

        for (Integer field : li) {
            sb.append(field.toString());
        }

        return sb.toString();
    }

    private class ODValidityChecker implements Callable<Integer> {

        private final int thNum;
        private final ConcurrentLinkedQueue<Candidate> candidates;
        private final ConcurrentSkipListSet<Candidate> toExpand;
        private final ArrayList<Column> dataset;

        private int localHoldCandidates = 0;
        private int localParsedCandidates = 0;

        public ODValidityChecker(int thNum, ConcurrentLinkedQueue<Candidate> candidates, ConcurrentSkipListSet<Candidate> toExpand, ArrayList<Column> dataset) {
            this.thNum = thNum;
            this.candidates = candidates;
            this.toExpand = toExpand;
            this.dataset = dataset;
        }

        @Override
        public Integer call() throws Exception {
            boolean found = false;
            Candidate cand = null;
            while (!candidates.isEmpty()) {
                cand = candidates.poll();
                found = false;
                if(cand == null){
                    return localParsedCandidates;
                }

                localParsedCandidates++;

                if (isValidCandidate(cand)) { // maybe generate just one and check also the inverse, i.e. A -> B, B -> A
                    this.localHoldCandidates++;
                    holdCandidate(cand);
                    found =  true;
                    System.out.println("TH[" + thNum + "] Found Candidates " + this.localHoldCandidates + " after " + this.localParsedCandidates);
                    System.out.println("TH[" + thNum + "] Comb size left: " + cand.getlHSFieldsList().size() + " Comb size right: " + cand.getrHSFieldsList().size()); //TESTING
                }

                toExpand.addAll(cand.expand(columnsIds, dataset, !found));

            }
            return localParsedCandidates;
        }

        private boolean isValidCandidate(Candidate candidate) {
            List<Integer> lhsList = candidate.getlHSFieldsList();
            List<Integer> rhsList = candidate.getrHSFieldsList();

            // PRUNING (eventuale), se va male: isValid
            //        lhsCol =
            //        rhsCol =
            return candidate.isValid(this.dataset);
        }

    }

}
