package it.unitn.disi.god.core;

import it.unitn.disi.god.utils.QuickSortAlong;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author francesco
 */
public class Candidate implements Comparable<Candidate>{

    private List<Integer> lHSFieldsList = null;
    private List<Long> lHSLongs = null;
    private List<Integer> rHSFieldsList = null;
    private List<Long> rHSLongs = null;
    private Long lDistinctValues;
    private Long rDistinctValues;

    private boolean checked = false;
    private int partialScanPosition = 0;

    public Candidate() {
        lHSFieldsList = new ArrayList<>();
        rHSFieldsList = new ArrayList<>();
        lHSLongs = new ArrayList<>();
        rHSLongs = new ArrayList<>();
    }

    private void setColumns(List<Column> alsc) {
        List<Tuple> lHSTuples = new ArrayList<>();
        List<Tuple> rHSTuples = new ArrayList<>();

        if (lHSFieldsList == null) {
            throw new IllegalStateException("lHSList is null! MONA!");
        }

        if (rHSFieldsList == null) {
            throw new IllegalStateException("rHSList is null! MONA!");
        }

        // sort & count distinct values
        int size = alsc.get(0).getSize();

        for (int i = 0; i < size; i++) {
            Tuple t = new Tuple();

            for (Integer e : lHSFieldsList) {
                t.add(alsc.get(e).get(i));
            }

            lHSTuples.add(t);
        }

        for (int i = 0; i < size; i++) {
            Tuple t = new Tuple();

            for (Integer e : rHSFieldsList) {
                t.add(alsc.get(e).get(i));
            }

            rHSTuples.add(t);
        }

        computeDistinctValuesAndSort(lHSTuples, rHSTuples);
    }

    private void computeDistinctValuesAndSort (List<Tuple> lHSTuples, List<Tuple> rHSTuples) {
        // compress tuples' lists
        HashMap<Tuple, Long> lIds = assignUniqIds(lHSTuples);
        lDistinctValues = 0L;
        for (Tuple lt : lHSTuples) {
            Long get = lIds.get(lt);

            if (get > lDistinctValues) {
                lDistinctValues = get;
            }

            lHSLongs.add(get);
        }

        HashMap<Tuple, Long> rIds = assignUniqIds(rHSTuples);
        rDistinctValues = 0L;
        for (Tuple rt : rHSTuples) {
            Long get = rIds.get(rt);

            if (get > rDistinctValues) {
                rDistinctValues = get;
            }

            rHSLongs.add(get);
        }

        // sort
        QuickSortAlong.quickSortAlong(lHSLongs, rHSLongs);
    }

    private HashMap<Tuple, Long> assignUniqIds(List<Tuple> lo) {
        ArrayList<Tuple> tmp = new ArrayList<>(lo.size());
        HashMap<Tuple, Long> hmcl = new HashMap<>(lo.size());
        tmp.addAll(lo);
        Collections.sort(tmp);
        Long l = 0L;
        Tuple prev = tmp.get(0);
        hmcl.put(prev, l);

        for (Tuple o : tmp) {
            if (!(prev.equals(o))) {
                prev = o;
                l++;
                hmcl.put(o, l);
            }
        }

        return hmcl;
    }

    public boolean isValid(List<Column> dataset) { // check only l \rightarrow_{<} r
        setColumns(dataset);

        // check distinct values, if distincValues(l) <= distinctValues(r)
        if (this.lDistinctValues > this.rDistinctValues) {
//            System.out.println("lHSFieldsList: "+lHSFieldsList + ", rHSFieldsList: "+rHSFieldsList);
            return false;
        }

        // check tuplewise (prova dura!)
        for (int i = 0; i < lHSLongs.size() - 1; i++) {
            for (int j = i + 1; j < lHSLongs.size(); j++) {
                if ((lHSLongs.get(i) < lHSLongs.get(j)) && !(rHSLongs.get(i) < rHSLongs.get(j))) {
                    return false;
                }
            }
        }

        return true;
    }

//    GETTERs & SETTERs
    public List<Integer> getlHSFieldsList() {
        return lHSFieldsList;
    }

    public void setlHSFieldsList(List<Integer> lHSFieldsList) {
        this.lHSFieldsList = lHSFieldsList;
    }

    public void setlHSFieldsList(Integer... i) {
        this.lHSFieldsList = Arrays.asList(i);
    }

    public List<Integer> getrHSFieldsList() {
        return rHSFieldsList;
    }

    public void setrHSFieldsList(List<Integer> rHSFieldsList) {
        this.rHSFieldsList = rHSFieldsList;
    }

    public void setrHSFieldsList(Integer... i) {
        this.rHSFieldsList = Arrays.asList(i);
    }

    public String getSignature() {
        return getLeftSignature()+" "+getRightSignature();
    }

    public String getLeftSignature() {
        StringBuilder sb = new StringBuilder();

        for (Integer field : lHSFieldsList) {
            sb.append(field.toString()).append(".");
        }

        return sb.toString();
    }

    public String getRightSignature() {
        StringBuilder sb = new StringBuilder();

        for (Integer field : rHSFieldsList) {
            sb.append(field.toString()).append(".");
        }

        return sb.toString();
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked() {
        this.checked =true;
    }

    public int getPartialScanPosition() {
        return partialScanPosition;
    }

    public void advancePartialScanPosition() {
        this.partialScanPosition++;
    }



    public List<Candidate> expand(List<Integer> allCols, List<Column> allColsValues, boolean bothSides) {
        List<Candidate> ret = new ArrayList<>();

        for (Integer col : allCols) {
            if (!this.lHSFieldsList.contains(col) && !this.rHSFieldsList.contains(col)) {
                Candidate cl = new Candidate();
                cl.setlHSFieldsList(new ArrayList<>(this.lHSFieldsList));
                cl.setrHSFieldsList(new ArrayList<>(this.rHSFieldsList));
                cl.lHSFieldsList.add(col);
                ret.add(cl);

                if(bothSides){
                    Candidate cr = new Candidate();
                    cr.setlHSFieldsList(new ArrayList<>(this.lHSFieldsList));
                    cr.setrHSFieldsList(new ArrayList<>(this.rHSFieldsList));
                    cr.rHSFieldsList.add(col);
                    ret.add(cr);
                }
            }
        }

        return ret;
    }

    @Override
    public int hashCode() {
        String sig = getSignature();
        return sig.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Candidate other = (Candidate) obj;

        return Objects.equals(this.getSignature(), other.getSignature());
    }

    @Override
    public int compareTo(Candidate o) {
        return this.getSignature().compareTo(o.getSignature());
    }




}
