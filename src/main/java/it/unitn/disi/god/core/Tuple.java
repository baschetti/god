/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.god.core;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author francesco
 */
public class Tuple implements Comparable<Tuple>{
    private final ArrayList<Long> tuple;
    
    public Tuple() {
        tuple = new ArrayList<>();
    }
    
    public void add(Long john) {
        tuple.add(john);
    }
    
    public Long get(int index) {
        return tuple.get(index);
    }

    @Override
    public int compareTo(Tuple o) {
        for (int i = 0; i < tuple.size(); i++) {
            int comp = tuple.get(i).compareTo(o.get(i));
            
            if (comp != 0) {
                return comp;
            }
        }
        
        return 0;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Tuple) {
            final Tuple tt = (Tuple) o;
            return this.compareTo(tt) == 0;
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.tuple);
        return hash;
    }
    
    @Override
    public String toString() {
        return tuple.toString();
    }
}
