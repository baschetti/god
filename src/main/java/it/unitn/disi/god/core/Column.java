/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.god.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author francesco
 */
public class Column implements Iterable<Long>{

    public enum type {

        LONG, DOUBLE, DATE, STRING, NULL
    };
    private static final Pattern datePattern = Pattern.compile("[1-9][0-9]{3}-[0-1][0-9]-[0-3][0-9]");
    private static final Pattern nullPattern = Pattern.compile("(\\?|-|[Nn][Uu][Ll]{2})");

    private final ArrayList<Long> values;
    private Long numDistinctValues;
    private final type columnType;
    private int nRows;

    public Column(List<String> lists) {
        type datatype = null;
        this.nRows = lists.size();

        for (String s : lists) {
            type newType = determineDataType(s);

            if (newType == type.NULL) {
                continue;
            }
            
            if (datatype == null) {
                datatype = newType;
            } else if (datatype != newType) {
                switch (newType) {
                    case LONG:
                        if (datatype == type.DATE) {
                            datatype = type.STRING;
                        }
                        break;
                    case DOUBLE:
                        if (datatype == type.DATE) {
                            datatype = type.STRING;
                        } else if (datatype == type.LONG) {
                            datatype = type.DOUBLE;
                        }
                        break;
                    case DATE:
                        if ((datatype == type.LONG) || (datatype == type.DOUBLE)) {
                            datatype = type.STRING;
                        }
                        break;
                    case STRING:
                        datatype = type.STRING;
                        break;
                }
            }

            if (datatype == type.STRING) {
                break;
            }
        }

        if (datatype == null) {
            datatype = type.NULL;
        }

        this.columnType = datatype;

        // cast
        switch (datatype) {
            case LONG:
                values = castAndSortLong(lists);
                break;
            case DOUBLE:
                values = castAndSortDouble(lists);
                break;
            case DATE:
                values = castAndSortDate(lists);
                break;
            default:
            case STRING:
                values = castAndSortString(lists);
                break;
            case NULL:
                values = castAndSortNull(lists);
                break;
        }
    }

    @Override
    public Iterator<Long> iterator() {
        return values.iterator();
    }
    
    private static type determineDataType(final String value) {
        if (value == null || isNull(value)) {
            return type.NULL;
        }

        if (isLong(value)) {
            return type.LONG;
        } else if (isDouble(value)) {
            return type.DOUBLE;
        } else if (isDate(value)) {
            return type.DATE;
        }

        return type.STRING;
    }

    private static boolean isNull(final String value) {
        return value.isEmpty() || nullPattern.matcher(value).matches();
    }
    
    private static boolean isDate(final String value) {
        return datePattern.matcher(value).matches();
    }

    private static boolean isDouble(final String value) {
        try {
            Double.parseDouble(value);
        } catch (final NumberFormatException e) {
            return false;
        }

        return true;
    }

    private static boolean isLong(final String value) {
        try {
            Long.parseLong(value);
        } catch (final NumberFormatException e) {
            return false;
        }

        return true;
    }

    private ArrayList<Long> castAndSortLong(List<String> lists) {
        ArrayList<Long> tmp = new ArrayList<>(lists.size());
        ArrayList<Long> out = new ArrayList<>(lists.size());

        for (String s : lists) {
//            System.out.println("s: *"+s+"*, isNull? "+isNull(s));
            tmp.add(isNull(s) ? null : Long.parseLong(s));
        }

        HashMap<Long, Long> map = assigneUniqIds(tmp);

        for (Long s : tmp) {
            out.add(map.get(s));
        }
        
        return out;
    }

    private ArrayList<Long> castAndSortDouble(List<String> lists) {
        ArrayList<Double> tmp = new ArrayList<>(lists.size());
        ArrayList<Long> out = new ArrayList<>(lists.size());

        for (String s : lists) {
            tmp.add(isNull(s) ? null : Double.parseDouble(s));
        }

        HashMap<Double, Long> map = assigneUniqIds(tmp);

        for (Double s : tmp) {
            out.add(map.get(s));
        }
        
        return out;
    }

    private ArrayList<Long> castAndSortDate(List<String> lists) {
        return castAndSortString(lists);
    }

    private ArrayList<Long> castAndSortString(List<String> lists) {
        ArrayList<String> tmp = new ArrayList<>(lists.size());
        ArrayList<Long> out = new ArrayList<>(lists.size());
        
        for (String s : lists) {
            tmp.add(isNull(s) ? null : s);
        }
        
        HashMap<String, Long> map = assigneUniqIds(tmp);
        
        for (String s : lists) {
            out.add(map.get(s));
        }
        
        return out;
    }
    
    private ArrayList<Long> castAndSortNull(List<String> lists) {
        return new ArrayList<>(Collections.nCopies(lists.size(), -1L));
    }

    private  <E extends Comparable<? super E>> HashMap<E, Long> assigneUniqIds(List<E> lo) {
        ArrayList<E> tmp = new ArrayList<>(lo.size());
        HashMap<E, Long> hmcl = new HashMap<>(lo.size());
        tmp.addAll(lo);
        Collections.sort(tmp, new NullComparator<E>());
        Long l = 0L;
        E prev = tmp.get(0);

        if (prev == null) {
            l = -1L;
        }

        hmcl.put(prev, l);
        
        for (E o : tmp) {
            if ((prev == null && o != null) || (prev != null && !(prev.equals(o)))) {
                prev = o;
                l++;
                hmcl.put(o, l);
            }
        }

        this.numDistinctValues = l+1;
        return hmcl;
    }

    public int getnRows() {
        return nRows;
    }

    public ArrayList<Long> getValues() {
        return values;
    }

    public Long getNumDistinctValues() {
        return numDistinctValues;
    }
    
    public int getSize() {
        return values.size();
    }
    
    public Long get(int index) {
        return values.get(index);
    }

    public type getColumnType() {
        return columnType;
    }
    
    public String getColumnTypeName() {
        return columnType.toString();
    }
    
    /**
     *
     * @param <T>
     */
    public static class NullComparator<T extends Comparable<? super T>> implements Comparator<T> {

        @Override
        public int compare(T o1, T o2) {
            
            if (o1 == null && o2 == null) {
                return 0;
            } else if (o1 == null) {
                return -1;
            }  else if (o2 == null) {
                return 1;
            }

            return ((T)o1).compareTo(o2);
        }
    }
}
