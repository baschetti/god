/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.god.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Matteo Lissandrini <ml@disi.unitn.eu>
 */
public class Permutations {

    private static <T> void swap(ArrayList<T> data, int i, int j) {
        T t = data.get(i);
        data.set(i, data.get(j));
        data.set(j, t);
    }

    public static <T extends Comparable<? super T>> boolean nextPerm(ArrayList<T> data) {
        // find the swaps
        int c = -1, d = data.size();
        for (int i = d - 2; i >= 0; i--) {
            if (data.get(i).compareTo(data.get(i + 1)) < 0) {
                c = i;
                break;
            }
        }

        if (c < 0) {
            return false;
        }

        int s = c + 1;
        for (int j = c + 2; j < d; j++) {
            if (data.get(j).compareTo(data.get(s)) < 0 && //
                    data.get(j).compareTo(data.get(c)) > 0) {
                s = j;
            }
        }

        // do the swaps
        swap(data, c, s);
        while (--d > ++c) {
            swap(data, c, d);
        }

        return true;
    }

    public static <T extends Comparable<? super T>> ArrayList<ArrayList<T>> permutations(ArrayList<T> d) {
        ArrayList<ArrayList<T>> result = new ArrayList<>();
        Collections.sort(d);
        do {
            result.add(new ArrayList<>(d));
        } while (nextPerm(d));
        return result;
    }

    public static <T extends Comparable<? super T>> ArrayList<ArrayList<T>> permutations(List<T> d) {
        ArrayList<T> tmp = new ArrayList<>(d.size());
        tmp.addAll(d);
        return permutations(tmp);
    }


//    public static void main(String[] args) {
//        String[] database = {"a", "b", "c"};
//
//        List db = new ArrayList<>();
//        db.addAll(Arrays.asList(database));
//
//        for (Object result1 : permutations(db)) {
//            System.out.println(result1);
//        }
//
//    }
}
