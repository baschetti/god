/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.god.utils;

import java.util.List;

/**
 *
 * @author Matteo Lissandrini <ml@disi.unitn.eu>
 */
public class QuickSortAlong {

    public static  <E extends Comparable<? super E>> void quickSortAlong(List<E> toSort, List<E> sortAlong) {
        quickSort(toSort, 0, (toSort.size() - 1), sortAlong);
    }

    private static <E extends Comparable<? super E>> void quickSort(List<E>  toSort, int start, int end, List<E> sortAlong) {

        int pivot = partition(toSort, start, end, sortAlong);

        if (start < pivot-1) {
            quickSort(toSort, start, pivot-1, sortAlong);
        }
        if (pivot < end) {
            quickSort(toSort, pivot, end, sortAlong);
        }

    }

    private static  <E extends Comparable<? super E>> int partition(List<E>  toSort, int start, int end, List<E> sortAlong) {

        E pivotValue = toSort.get((start + end) / 2);

        while (start <= end) {
            while (toSort.get(start).compareTo(pivotValue) < 0 ) {
                start++;
            }
            while (toSort.get(end).compareTo(pivotValue) > 0) {
                end--;
            }
            if (start <= end) {
                swap(toSort, start, end, sortAlong);
                start++;
                end--;
            }
        }

        return start;

    }

    private static  <E extends Comparable<? super E>> void swap(List<E>  arr, int i, int j, List<E> sortAlong) {
        if (i != j && i >= 0 && j >= 0 && i < arr.size() && j < arr.size()) {
            final E temp1 = arr.get(i);
            arr.set(i, arr.get(j));
            arr.set(j, temp1);

            final E temp2 = sortAlong.get(i);
            sortAlong.set(i, sortAlong.get(j));
            sortAlong.set(j, temp2);
        }
    }




}
