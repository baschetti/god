package it.unitn.disi.god.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author francesco
 * @param <E>
 */
public class Combinations<E> implements Iterable<List<E>> {

    private final List<E> elements;
    private final int combSize;

    public Combinations(List<E> l, int combSize) {
        this.elements = l;
        this.combSize = combSize;
    }

//    public static void main(String[] args) {
////        test();
//        List<String> a = new ArrayList();
//        a.add("a");
//        a.add("b");
//        a.add("c");
//        a.add("d");
//        a.add("e");
//
//        Combinations comb = new Combinations(a, 2);
//
//        for (List comb1 : comb) {
//            System.out.println(comb1);
//        }
//    }

    public static void test() {

        int neighboursDim = 5;
        int l = 2;
        int[] currentCombination = new int[neighboursDim];

        for (int i = 0; i < neighboursDim; i++) {
            currentCombination[i] = i;
        }

        currentCombination[l - 1] = l - 1 - 1;

        do {
            if (currentCombination[l - 1] == (neighboursDim - 1)) {
                int i = l - 1 - 1;

                while (currentCombination[i] == (neighboursDim - l + i)) {
                    i--;
                }

                currentCombination[i]++;

                for (int j = i + 1; j < l; j++) {
                    currentCombination[j] = currentCombination[i] + j - i;
                }
            } else {
                currentCombination[l - 1]++;
            }

            System.out.println("Print currentCombination:");
            for (int i = 0; i < l; i++) {
                System.out.print(currentCombination[i] + " ");
            }
            System.out.println();

        } while (!((currentCombination[0] == (neighboursDim - 1 - l + 1)) && (currentCombination[l - 1] == (neighboursDim - 1))));

    }

    @Override
    public Iterator<List<E>> iterator() {
        return new CombinationsIterator(this.elements.size(), this.combSize);
    }

    private class CombinationsIterator implements Iterator<List<E>> {

        private final int[] currentCombination;
        private final int neighboursDim;
        private final int l;

        public CombinationsIterator(int size, int combSize) {
            this.neighboursDim = size;
            currentCombination = new int[size];

            for (int i = 0; i < this.neighboursDim; i++) {
                this.currentCombination[i] = i;
            }

            this.l = combSize;
            currentCombination[this.l - 1] = this.l - 1 - 1;
        }

        @Override
        public boolean hasNext() {
            return (!((currentCombination[0] == (neighboursDim - 1 - l + 1))
                    && (currentCombination[l - 1] == (neighboursDim - 1))));
        }

        @Override
        public List<E> next() {
            if (currentCombination[l - 1] == (neighboursDim - 1)) {
                int i = l - 1 - 1;

                while (currentCombination[i] == (neighboursDim - l + i)) {
                    i--;
                }

                currentCombination[i]++;

                for (int j = i + 1; j < l; j++) {
                    currentCombination[j] = currentCombination[i] + j - i;
                }
            } else {
                currentCombination[l - 1]++;
            }

            List<E> r = new ArrayList<E>();

            for (int i = 0; i < combSize; i++) {
                r.add(elements.get(currentCombination[i]));
            }

            return r;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
