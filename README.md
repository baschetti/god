# Big Data Profiling course: GOD project

## Authors:

 - Francesco Asnicar University of Trento f.asnicar@unitn.it
 - Matteo Lissandrini University of Trento ml@disi.unitn.eu


### Content

This repository contains the development of an algorithm for the discovery of order dependencies.
The algorithm has been developed in the context of the “Big Data Profiling” course.
We propose two alternative brute force approaches for th task of generating candidate Order Dependencies, 
and some compressions strategies in order to speed up the check for the validity of such candidates.

The repostory contains two branches
 
 - `comb` for the combinatorial solution
 - `master` with the apriori method
